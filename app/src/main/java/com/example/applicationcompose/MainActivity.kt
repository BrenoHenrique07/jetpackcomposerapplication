package com.example.applicationcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.applicationcompose.ui.theme.ApplicationComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ApplicationComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    perfil("Android")
                }
            }
        }
    }
}

@Composable
fun perfil(name: String) {
    var flag by remember { mutableStateOf(false) }
    Card(
        modifier = Modifier
            .padding(8.dp),
        elevation = 4.dp
    ) {
        Column(
            modifier = Modifier
                .padding(8.dp)
                .fillMaxSize()
        ) {
            Image(
                painter = painterResource(id = R.drawable.pizza),
                contentDescription = "Pizza Picture",

                modifier = Modifier
                    .clip(CircleShape)
                    .align(Alignment.CenterHorizontally)
            )
            Row(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
            ) {
                Text(
                    text = "Portuguesa",
                    style = MaterialTheme.typography.body1,
                    fontSize = 22.sp
                )
                Text (
                    text = if(flag) "--" else "+",
                    fontSize = 22.sp,

                    modifier = Modifier
                        .padding(horizontal = 8.dp)
                        .clickable { flag = !flag }
                )
            }
            AnimatedVisibility(
                visible = flag,
                enter = fadeIn(initialAlpha = 0f) + expandVertically(),
                exit = fadeOut(animationSpec = tween(durationMillis = 200)) + shrinkVertically()
            ) {
                Text(text = "teste")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ApplicationComposeTheme {
        perfil("Android")
    }
}